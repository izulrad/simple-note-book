package com.izulrad.simplenotebook.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.model.Note
import com.izulrad.simplenotebook.util.*
import com.parse.ParseException
import kotlinx.android.synthetic.main.activity_note_edit.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread


class NoteEditActivity : AppCompatActivity(), NoteService, AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_edit)

        init()
    }

    private fun init() {
        val note =
            if (intent.hasExtra(Note.UUID_KEY)) {
                val uuid = intent.extras.getString(Note.UUID_KEY)
                logi("edit note with uuid=$uuid")
                with(Note.query) {
                    fromLocalDatastore()
                    whereEqualTo(Note.UUID_KEY, uuid)
                    first.apply { edit_text_note.setText(text, TextView.BufferType.EDITABLE) }
                }
            } else {
                logi("create new note")
                Note().apply { generateUuid() }
            }

        button_save_note.setOnClickListener { saveNote(note) }
    }

    private fun saveNote(note: Note) {
        progress_bar_edit_note.show()
        form_edit_note.hide()

        async() {
            var parseException: ParseException? = null

            note.text = edit_text_note.trimString
            note.tags = textToTagsList(note.text)

            try {
                pinAsNoSync(note)
            } catch(pe: ParseException) {
                parseException = pe
            }

            uiThread {
                if (parseException == null) {
                    logi("note created/edited")
                    finish()
                } else {
                    loge(parseException)
                    progress_bar_edit_note.gone()
                    form_edit_note.show()
                    Snackbar
                        .make(form_edit_note, parseException!!.message!!, Snackbar.LENGTH_LONG)
                        .show()
                }
            }
        }
    }
}
