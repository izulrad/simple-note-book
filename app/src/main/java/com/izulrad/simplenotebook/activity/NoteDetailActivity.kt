package com.izulrad.simplenotebook.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.model.Note
import kotlinx.android.synthetic.main.activity_note_detail.*
import org.jetbrains.anko.AnkoLogger

class NoteDetailActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_detail)

        init()
    }

    private fun init() {
        val uuid = intent.extras.getString(Note.UUID_KEY)

        val note = with(Note.query) {
            fromLocalDatastore()
            whereEqualTo(Note.UUID_KEY, uuid)
            first
        }

        text_view_detail_note.text = note.text
    }
}
