package com.izulrad.simplenotebook.activity.login

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.util.*
import com.parse.ParseException
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.jetbrains.anko.AnkoLogger
import java.util.*

class SignUpActivity : AppCompatActivity(), AnkoLogger {

    private val mErrors: MutableList<() -> EditText?> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        init()
    }

    private fun init() {
        edit_text_re_password.setOnEditorActionListener { textView, i, keyEvent ->
            signUp()
            true
        }

        text_view_sign_up.setOnClickListener { signUp() }
    }

    private fun signUp() {
        form_sign_up.hide()
        progress_bar_sign_up.show()

        val username = edit_text_username.trimString
        val email = edit_text_email.trimString
        val password = edit_text_password.trimString
        val rePassword = edit_text_re_password.trimString

        logi("sign up with username=$username")

        if (username.isEmpty())
            addError(edit_text_username, getString(R.string.error_can_not_be_empty))

        if (email.isEmpty())
            addError(edit_text_email, getString(R.string.error_can_not_be_empty))

        if (password != rePassword)
            mErrors.add {
                edit_text_re_password.apply {
                    clearText()
                    error = getString(R.string.error_passwords_must_match)
                }
            }

        if (mErrors.size == 0) {
            with(ParseUser.getCurrentUser()) {
                this.username = username
                this.email = email
                setPassword(password)
                signUpInBackground { e: ParseException? ->
                    if (e == null) {
                        logi("sign up success")
                        setResult(Activity.RESULT_OK)
                        finish()
                    } else {
                        loge(e)
                        progress_bar_sign_up.gone()
                        form_sign_up.show()

                        when (e.code) {
                            ParseException.USERNAME_TAKEN ->
                                addError(edit_text_username,
                                    getString(R.string.error_username_already_taken))

                            ParseException.EMAIL_TAKEN ->
                                addError(edit_text_email,
                                    getString(R.string.error_email_already_taken))

                            ParseException.INVALID_EMAIL_ADDRESS ->
                                addError(edit_text_email, getString(R.string.error_invalid_email))

                            else ->
                                mErrors.add {
                                    Snackbar
                                        .make(form_sign_up, e.message!!, Snackbar.LENGTH_LONG)
                                        .show()
                                    null
                                }
                        }
                        showErrors()
                    }
                }
            }
        } else showErrors()
    }

    private fun addError(et: EditText, err: String) = mErrors.add { et.apply { error = err } }

    private fun showErrors() {
        for ((k, v) in mErrors.withIndex()) {
            if (k == mErrors.lastIndex) v.invoke()?.requestFocus()
            else v.invoke()
        }

        mErrors.clear()
    }
}