package com.izulrad.simplenotebook.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.activity.login.SignInActivity
import com.izulrad.simplenotebook.model.Note
import com.izulrad.simplenotebook.util.*
import com.parse.ParseAnonymousUtils
import com.parse.ParseException
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_note_list.*
import kotlinx.android.synthetic.main.content_note_list.*
import org.jetbrains.anko.*
import kotlin.properties.Delegates

class NoteListActivity : AppCompatActivity(), AnkoLogger, NoteService {

    private var mAdapter by Delegates.notNull<NotesListAdapter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_list)
        setSupportActionBar(toolbar)

        init()
    }

    override fun onResume() {
        super.onResume()

        syncNotes()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == SignInActivity.RQ) {
            grubAnonymousNotes()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_note_list, menu)

        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val isAnonymous = ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())
        val hasConnect = hasInternetConnection

        menu?.findItem(R.id.action_sign_in)?.setVisible(isAnonymous && hasConnect)
        menu?.findItem(R.id.action_sign_out)?.setVisible(!isAnonymous && hasConnect)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_sign_in ->
                startActivityForResult<SignInActivity>(SignInActivity.RQ)

            R.id.action_sign_out -> {
                progress_bar_note_list.show()
                ParseUser.logOutInBackground { e: ParseException? ->
                    mAdapter.clear()
                    progress_bar_note_list.hide()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?,
                                     menuInfo: ContextMenu.ContextMenuInfo?) {
        menu?.add(R.string.action_edit_note)
        menu?.add(R.string.action_remove_note)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val pos = (item?.menuInfo as AdapterView.AdapterContextMenuInfo).position

        when (item?.title) {
            getString(R.string.action_edit_note) ->
                startActivity<NoteEditActivity>(Note.UUID_KEY to mAdapter.getItem(pos).uuid)

            getString(R.string.action_remove_note) ->
                alert(R.string.question_r_u_sure, R.string.action_remove_note) {
                    positiveButton {
                        val note = mAdapter.getItem(pos)
                        mAdapter.remove(note)
                        pinAsDeleted(note)
                    }
                    negativeButton { }
                }.show()
        }

        return true
    }

    private fun init() {
        mAdapter = NotesListAdapter(applicationContext)

        registerForContextMenu(list_view_notes)
        list_view_notes.adapter = mAdapter
        list_view_notes.setOnItemClickListener { parent, view, pos, id ->
            startActivity<NoteDetailActivity>(Note.UUID_KEY to mAdapter.getItem(pos).uuid)
        }

        fab_add_new_note.setOnClickListener {
            startActivity<NoteEditActivity>()
        }

        search_view_by_tags.setOnCloseListener({
            syncNotes()
            false
        })

        search_view_by_tags.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                progress_bar_note_list.show()
                mAdapter.clear()

                async() {
                    val result = findNotesByTags(textToTagsList(query!!))

                    uiThread {
                        mAdapter.addAll(result)
                        progress_bar_note_list.hide()
                    }
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    private fun syncNotes() {
        logi("syncNotes")
        progress_bar_note_list.show()
        mAdapter.clear()

        val isAnonymous = ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())
        val useCloud = hasInternetConnection && !isAnonymous
        logi("useCloud=$useCloud")

        /*Local data always win*/
        async() {
            if (useCloud) {
                storeNotesToCloud()
                updateNotesFromCloud()
            }
            val result = findNotes()

            uiThread {
                mAdapter.addAll(result)
                progress_bar_note_list.hide()
            }
        }
    }
}
