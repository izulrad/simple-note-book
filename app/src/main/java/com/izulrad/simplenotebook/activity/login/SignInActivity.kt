package com.izulrad.simplenotebook.activity.login

import android.app.Activity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.util.*
import com.parse.ParseException
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivity

class SignInActivity : AppCompatActivity(), AnkoLogger {

    companion object {
        val RQ = 4201
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

       init()
    }

    private fun init() {
        edit_text_password.setOnEditorActionListener { textView, i, keyEvent ->
            login()
            true
        }

        button_sign_in.setOnClickListener { login() }

        text_view_sign_up.setOnClickListener {
            startActivity<SignUpActivity>()
            finish()
        }
    }

    private fun login() {
        form_sign_in.hide()
        progress_bar_sign_in.show()

        val username = edit_text_username.trimString
        val password = edit_text_password.trimString
        logi("sign in for username=$username")

        ParseUser.logInInBackground(username, password) { user, e: ParseException? ->
            if (e == null) {
                logi("sign in success")
                setResult(Activity.RESULT_OK)
                finish()
            } else {
                loge(e)
                progress_bar_sign_in.gone()
                form_sign_in.show()

                if (e.code == ParseException.OBJECT_NOT_FOUND) {
                    edit_text_password.clearText()
                    edit_text_password.requestFocus()
                    Snackbar
                        .make(
                            form_sign_in,
                            getString(R.string.text_forgot_password),
                            Snackbar.LENGTH_INDEFINITE
                        )
                        .setAction(getString(R.string.action_reset_password), {
                            startActivity<ResetPasswordActivity>()
                            finish()
                        })
                        .show()
                } else Snackbar.make(form_sign_in, e.message!!, Snackbar.LENGTH_LONG).show()
            }
        }
    }
}

