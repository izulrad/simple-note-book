package com.izulrad.simplenotebook.activity.login

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.util.*
import com.parse.ParseException
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.jetbrains.anko.AnkoLogger

class ResetPasswordActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        init()
    }

    private fun init() {
        edit_text_email.setOnEditorActionListener { textView, i, keyEvent ->
            resetPassword()
            true
        }

        button_reset_password.setOnClickListener { resetPassword() }
    }

    private fun resetPassword() {
        form_reset_password.hide()
        progress_bar_reset_password.show()

        val email = edit_text_email.trimString
        logi("reset password for email=$email")

        ParseUser.requestPasswordResetInBackground(email) { e: ParseException? ->
            progress_bar_reset_password.gone()
            form_reset_password.show()

            if (e == null) {
                logi("email has been sent")
                edit_text_email.clearText()
                Snackbar
                    .make(
                        form_reset_password,
                        getString(R.string.text_check_email),
                        Snackbar.LENGTH_INDEFINITE
                    )
                    .show()
            } else {
                loge(e)
                edit_text_email.error = when (e.code) {
                    ParseException.INVALID_EMAIL_ADDRESS -> getString(R.string.error_invalid_email)
                    ParseException.EMAIL_MISSING -> getString(R.string.error_can_not_be_empty)
                    ParseException.EMAIL_NOT_FOUND -> getString(R.string.error_email_not_found)
                    else -> e.message
                }
            }
        }
    }
}

