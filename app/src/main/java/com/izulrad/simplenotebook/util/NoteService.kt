package com.izulrad.simplenotebook.util

import com.izulrad.simplenotebook.model.Note
import com.parse.ParseAnonymousUtils
import com.parse.ParseException
import com.parse.ParseObject
import com.parse.ParseUser
import org.jetbrains.anko.AnkoLogger

interface NoteService : AnkoLogger

private val defaultPins: List<String> = listOf(
    ParseObject.DEFAULT_PIN, Note.ANONYMOUS, Note.NO_SYNC, Note.SYNC)

private fun clearAllPins(note: Note) {
    defaultPins.forEach { note.unpin(it) }
}

fun NoteService.pinAsNoSync(note: Note) {
    logi("pinAsNoSync start $note")
    if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) note.pin(Note.ANONYMOUS)
    note.pin(Note.NO_SYNC)
    logi("pinAsNoSync end")
}

fun NoteService.pinAsDeleted(note: Note) {
    logi("pinAsDeleted start $note")
    clearAllPins(note)
    if (note.objectId != null) {
        note.isDeleted = true
        note.pin(Note.NO_SYNC)
    }
    logi("pinAsDeleted end")
}

fun NoteService.grubAnonymousNotes() {
    logi("grubAnonymousNotes start")
    val anonymousNotes = with(Note.query) {
        ignoreACLs()
        fromPin(Note.ANONYMOUS)
        find().apply { logi("anonymousNotes items=${this.size}") }
    }

    val userNotes = anonymousNotes.map {
        Note().apply {
            generateUuid()
            tags = it.tags
            text = it.text
        }
    }

    ParseObject.pinAll(Note.NO_SYNC, userNotes)
    ParseObject.unpinAll(Note.ANONYMOUS)
    logi("grubAnonymousNotes end")
}

fun NoteService.storeNotesToCloud() {
    logi("storeNotesToCloud start")

    val noSyncNotes = with(Note.query) {
        fromPin(Note.NO_SYNC)
        find().apply { logi("noSyncNotes items=${this.size}") }
    }

    ParseObject.saveAll(noSyncNotes)
    ParseObject.unpinAll(Note.NO_SYNC, noSyncNotes)
    logi("storeNotesToCloud end")
}

fun NoteService.updateNotesFromCloud() {
    logi("updateNotesFromCloud start")
    val lastUpdate = with(Note.query) {
        fromLocalDatastore()
        addDescendingOrder("updatedAt")
        try {
            first.updatedAt
        } catch(e: ParseException) {
            null
        }
    }

    logi("lastUpdate=$lastUpdate")

    val loadedNotes: List<Note> = with(Note.query) {
        if (lastUpdate != null) whereGreaterThan("updatedAt", lastUpdate)
        find()
    }

    ParseObject.pinAll(Note.SYNC, loadedNotes)
    logi("updateNotesFromCloud end")

}

fun NoteService.findNotes(): List<Note> {
    logi("findNotes start")
    return with(Note.query) {
        fromLocalDatastore()
        whereEqualTo(Note.IS_DELETED_KEY, false)
        addDescendingOrder(Note.LOCAL_CREATED_AT_KEY)
        find().apply { logi("found=${this.size}") }
    }
}

fun NoteService.findNotesByTags(tags: List<String>): List<Note> {
    logi("findNotesByTags start for tags=$tags")
    return with(Note.query) {
        fromLocalDatastore()
        whereEqualTo(Note.IS_DELETED_KEY, false)
        addDescendingOrder(Note.LOCAL_CREATED_AT_KEY)
        whereContainsAll(Note.TAGS_KEY, tags)
        find().apply { logi("found=${this.size}") }
    }
}

