package com.izulrad.simplenotebook.util

import android.view.View
import android.widget.EditText
import android.widget.TextView

fun View.show() = setVisibility(View.VISIBLE)

fun View.hide() = setVisibility(View.INVISIBLE)

fun View.gone() = setVisibility(View.GONE)

val EditText.trimString: String get() = text.trim().toString()

fun EditText.clearText() = setText("", TextView.BufferType.EDITABLE)

