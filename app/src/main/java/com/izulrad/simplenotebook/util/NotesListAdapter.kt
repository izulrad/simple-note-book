package com.izulrad.simplenotebook.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.izulrad.simplenotebook.R
import com.izulrad.simplenotebook.model.Note

class NotesListAdapter(ctx: Context) : ArrayAdapter<Note>(ctx, 0) {

    companion object {
        val MAX_PREVIEW_LENGTH = 100
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val item: Note = getItem(position)

        val view: View = convertView ?: LayoutInflater.from(context).inflate(R.layout.note_list_item, parent, false)
            .apply {
                tag = ViewHolder(
                    findViewById(R.id.text_view_tags) as TextView,
                    findViewById(R.id.text_view_note) as TextView)
            }


        val holder = view.tag as ViewHolder
        holder.tags.text = item.tags.joinToString(separator = " ")
        holder.note.text =
            if (item.text.length < MAX_PREVIEW_LENGTH) item.text
            else item.text.subSequence(0, MAX_PREVIEW_LENGTH)

        return view
    }

    private class ViewHolder(val tags: TextView, val note: TextView)
}

