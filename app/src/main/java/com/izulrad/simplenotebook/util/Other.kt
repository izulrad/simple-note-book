package com.izulrad.simplenotebook.util

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import com.parse.ParseException
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info


val AppCompatActivity.hasInternetConnection: Boolean
    get() = with(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager) {
        activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

private val TAG = "!042 "

fun AnkoLogger.logi(message: Any?) {
    info(TAG + message)
}

fun AnkoLogger.loge(message: Any?){
    error(TAG + message)
}

fun AnkoLogger.loge(e: ParseException?) = loge("parse exception ${e?.code} ${e?.message}")

fun textToTagsList(text: String): List<String> {
    return Regex("#\\S+").findAll(text).map { mr: MatchResult -> mr.value }.toSet().toList()
}