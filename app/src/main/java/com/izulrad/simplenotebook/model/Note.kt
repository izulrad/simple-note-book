package com.izulrad.simplenotebook.model

import com.parse.ParseClassName
import com.parse.ParseObject
import com.parse.ParseQuery
import java.util.*

@ParseClassName("Note")
class Note : ParseObject() {

    companion object {
        val UUID_KEY = "uuid"
        val TAGS_KEY = "tags"
        val TEXT_KEY = "text"
        val IS_DELETED_KEY = "isDeleted"
        val LOCAL_CREATED_AT_KEY = "localCreatedAt"

        val SYNC = "SYNC_NOTES_GROUP"
        val NO_SYNC = "NO_SYNC_NOTES_GROUP"
        val ANONYMOUS = "ANONYMOUS_NOTES_GROUP"

        val query: ParseQuery<Note>
            get() = ParseQuery.getQuery(Note::class.java)
    }

    val uuid: String
        get() = getString(UUID_KEY)

    var tags: List<String>
        get() = getList(TAGS_KEY)
        set(value) = put(TAGS_KEY, value)

    var text: String
        get() = getString(TEXT_KEY)
        set(value) = put(TEXT_KEY, value)

    val localCreatedAt: Date
        get() = getDate(LOCAL_CREATED_AT_KEY)

    var isDeleted: Boolean
        get() = getBoolean(IS_DELETED_KEY)
        set(value) = put(IS_DELETED_KEY, value)

    fun generateUuid() {
        put(UUID_KEY, UUID.randomUUID().toString())
        put(LOCAL_CREATED_AT_KEY, Date())
        isDeleted = false
    }

    override fun toString(): String {
        return "$uuid $tags $text"
    }
}
