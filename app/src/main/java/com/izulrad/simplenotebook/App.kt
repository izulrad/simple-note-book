package com.izulrad.simplenotebook

import android.app.Application
import com.izulrad.simplenotebook.model.Note
import com.parse.Parse
import com.parse.ParseACL
import com.parse.ParseObject
import com.parse.ParseUser
import org.jetbrains.anko.AnkoLogger

class App : Application(), AnkoLogger{
    override fun onCreate() {
        super.onCreate()

        ParseObject.registerSubclass(Note::class.java)
        Parse.enableLocalDatastore(this)
        Parse.initialize(this)
        ParseUser.enableAutomaticUser()
        ParseACL.setDefaultACL(ParseACL(), true)
    }
}

